package com.zajac.adam;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Client controller
 * Created by zajac on 10.02.2018.
 */
@RestController
public class InstagramController {

    private final OAuth2ClientContext oauth2ClientContext;
    private final EurekaClient discoveryClient;
    private final String myUrl = "http://localhost:8899/user/";
    private RestTemplate restTemplate;
    @Value("${instagram.media.mediaInfoUri}")
    private String mediaInfoUri;

    @Autowired
    public InstagramController(@Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext,
                               @Qualifier("eurekaClient") EurekaClient discoveryClient) {
        this.restTemplate = new RestTemplate();
        this.oauth2ClientContext = oauth2ClientContext;
        this.discoveryClient = discoveryClient;
    }

    @GetMapping("/user/self")
    public ResponseEntity<Map<String, String>> user(Principal principal) {
        Map<String, String> userDetails;
        try {
            userDetails = getUserDetails(principal);
        } catch (Exception e) {
            userDetails = new LinkedHashMap<>();
            Boolean aBoolean = false;
            userDetails.put("authenticated", aBoolean.toString());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userDetails);
        }
        return ResponseEntity.ok().header("Domain", "Instagram").body(userDetails);
    }

    @RequestMapping("/user/media")
    public ResponseEntity<String> getFullMedia() {
        final String tokenValue = oauth2ClientContext.getAccessToken().getValue();
        final String urlWithAccessToken = mediaInfoUri + "?access_token=" + tokenValue;

        return restTemplate.getForEntity(urlWithAccessToken, String.class);
    }

    @RequestMapping("/user/images-links")
    public @ResponseBody
    Set<String> getLinksToImages() {
        final String tokenValue = oauth2ClientContext.getAccessToken().getValue();
        final String urlWithAccessToken = mediaInfoUri + "?access_token=" + tokenValue;
        final ResponseEntity<String> forEntity = restTemplate.getForEntity(urlWithAccessToken, String.class);

        return changeBodyToImageUrls(forEntity.getBody());
    }

    @GetMapping("/")
    public void redirectToMainDomain(HttpServletResponse servletResponse) throws IOException {
        servletResponse.sendRedirect(myUrl);
    }

    private Map<String, String> getUserDetails(Principal principal) {
        Map<String, String> map = new LinkedHashMap<>();
        String username = "username", fullName = "full_name", profilePictureUrl = "profile_picture";
        final Boolean authenticated = ((OAuth2Authentication) principal).getUserAuthentication().isAuthenticated();
        map.put("authenticated", authenticated.toString());
        map.put(username, getUserDetailFromPrincipal(principal, username));
        map.put(fullName, getUserDetailFromPrincipal(principal, fullName));
        map.put(profilePictureUrl + "_url", getUserDetailFromPrincipal(principal, profilePictureUrl));
        return map;
    }


    private String getUserDetailFromPrincipal(Principal principal, String detail) {
        return ((LinkedHashMap) ((LinkedHashMap) ((OAuth2Authentication) principal).getUserAuthentication().getDetails()).get("data")).get(detail).toString();
    }

    private Set<String> changeBodyToImageUrls(String body) {
        ObjectMapper mapper = new ObjectMapper();
        Set<String> imageUrls = new HashSet<>();
        try {
            JsonNode root = mapper.readTree(body);
            imageUrls = root.findValues("standard_resolution")
                    .stream().map(s -> s.get("url").textValue()).collect(Collectors.toSet());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageUrls;
    }

    //To unit tests
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
