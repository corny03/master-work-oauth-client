package com.zajac.adam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.Filter;

/**
 * Oauth connection configuration
 * Created by zajac on 10.02.2018.
 */
@Configuration
public class WebServiceConfiguration extends WebSecurityConfigurerAdapter {

    private final OAuth2ClientContext oauth2ClientContext;

    @Autowired
    public WebServiceConfiguration(@Qualifier("oauth2ClientContext") OAuth2ClientContext oauth2ClientContext) {
        this.oauth2ClientContext = oauth2ClientContext;
    }

    public OAuth2ClientContext getOauth2ClientContext() {
        return oauth2ClientContext;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http

                .authorizeRequests()
                .antMatchers("/**")
                .permitAll()
                .anyRequest()
                .authenticated().and()
                .addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class)
                .logout()
                .and()
                .csrf().disable()
//                .and()
//                .csrf()
//                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        ;
    }

    private Filter ssoFilter() {
        OAuth2ClientAuthenticationProcessingFilter instagramFilter =
                new OAuth2ClientAuthenticationProcessingFilter("/login/instagram");
        OAuth2RestTemplate instagramTemplate =
                new OAuth2RestTemplate(instagram(), oauth2ClientContext);
        instagramFilter.setRestTemplate(instagramTemplate);
        UserInfoTokenServices tokenServices =
                new UserInfoTokenServices(instagramResource().getUserInfoUri(), instagram().getClientId());
        tokenServices.setRestTemplate(instagramTemplate);
        instagramFilter.setTokenServices(tokenServices);
        return instagramFilter;
    }

    @Bean
    @ConfigurationProperties("instagram.client")
    public AuthorizationCodeResourceDetails instagram() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @ConfigurationProperties("instagram.resource")
    public ResourceServerProperties instagramResource() {
        return new ResourceServerProperties();
    }

    @Bean
    public FilterRegistrationBean oauth2ClientFilterRegistration(
            OAuth2ClientContextFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);
        registration.setOrder(-100);
        return registration;
    }
}
